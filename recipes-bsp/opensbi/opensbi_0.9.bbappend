FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

BRANCH = "v0.9_incoresemi"
SRCREV = "e1628bc387af959d77793f79b426f87374e76202"
SRC_URI = "git://gitlab.com/incoresemi/software/opensbi.git;branch=${BRANCH};protocol=https; \
	file://0001-Makefile-Don-t-specify-mabi-or-march.patch \
"

SRC_URI[sha256sum] = "a867b16f7af64a8a4b7bd1400af8d0f705678a163ae5b8a1221af298f6c6f420"

LIC_FILES_CHKSUM = "file://COPYING.BSD;md5=42dd9555eb177f35150cf9aa240b61e5"

EXTRA_OEMAKE_append += " FW_FDT_PATH=${DEPLOY_DIR_IMAGE}/${RISCV_SBI_FDT}"
