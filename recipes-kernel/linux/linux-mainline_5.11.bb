require recipes-kernel/linux/linux-mainline-common.inc

LINUX_VERSION ?= "5.11"
KERNEL_VERSION_SANITY_SKIP="1"

# Tag - v5.11
BRANCH = "v5.11_incoresemi"
SRCREV = "399fcd70522ec4df2b73d2aff1493d114208ecae"
SRC_URI = " \
	git://gitlab.com/incoresemi/software/linux.git;branch=${BRANCH};protocol=https; \
	file://defconfig;name=defconfig \
"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

COMPATIBLE_MACHINE = "chromite-h"
INITRAMFS_IMAGE = "riscv-initramfs-image"
