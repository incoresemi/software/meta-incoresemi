COMPATIBLE_MACHINE = "chromite-h"
EXTRA_IMAGEDEPENDS = ""
USE_DEVFS = "0"
inherit extrausers

EXTRA_USERS_PARAMS = "usermod -P incore root;"

PACKAGE_INSTALL_append = " vim-tiny"
